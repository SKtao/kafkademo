package com.ktao.msg.listener;

import com.ktao.msg.handler.TestSendHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 22:03
 * @Description:
 */
@Component
public class TestListener extends AbstractMessageListener {

    @Resource
    private TestEventHandler testEventHandler;

    @Value("${kafka.topic.consumer.test}")
    private String topic;
    @Value("${kafka.groupId.test_group}")
    private String groupId;

    @Override
    public String getGroupId() {
        return groupId;
    }

    @Override
    public String getTopic() {
        return topic;
    }

    @Override
    public CustomBatchRecordHandler getCustomBatchRecordHandler() {
        return testEventHandler;
    }
}
