package com.ktao.msg.listener;

import com.ktao.msg.handler.AbstractMessageHandler;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.stereotype.Component;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 22:09
 * @Description:
 */
@Component
public class TestEventHandler extends AbstractMessageHandler{

    @Override
    protected void handleBatchMessage(ConsumerRecords<String, String> records) throws Exception {
        for (ConsumerRecord<String, String> record : records){
            System.out.println("消费：" +record.toString());
        }
    }
}
