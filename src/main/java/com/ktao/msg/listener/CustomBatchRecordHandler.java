package com.ktao.msg.listener;

import org.apache.kafka.clients.consumer.ConsumerRecords;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 21:54
 * @Description:
 */
public interface CustomBatchRecordHandler {
    void callbackBatch(ConsumerRecords<String, String> records);
}
