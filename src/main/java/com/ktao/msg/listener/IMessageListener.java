package com.ktao.msg.listener;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 21:53
 * @Description:
 */
public interface IMessageListener {
    String PROPERTY_FILE_PATH = "kafka.properties";

    String getGroupId();

    String getTopic();

    CustomBatchRecordHandler getCustomBatchRecordHandler();
}
