package com.ktao.msg.listener;

import com.ktao.msg.consumer.MsgConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

import static org.apache.kafka.common.requests.DeleteAclsResponse.log;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 21:57
 * @Description:
 */
@Component
public abstract class AbstractMessageListener implements IMessageListener {
    private Properties consumerProperties;
    private MsgConsumer consumer;

    public AbstractMessageListener(){}

    public void init()throws IOException {
        log.info("MsgConsumer:" + this.getTopic() + "开始初始化");
        this.consumerProperties = PropertiesLoaderUtils.loadAllProperties("kafka.properties");
        this.consumerProperties.put("group.id", this.getGroupId());
        try {
            this.consumer = new MsgConsumer(this.consumerProperties);
        } catch (Exception var2) {
            log.info("MsgConsumer:" + this.getTopic() + "初始化失败", var2);
        }
    }
    @PostConstruct
    protected void startListener()throws Exception {
        this.init();
        if (consumer == null){
            log.info("KafkaConsumer: "+ this.getTopic() + "为空，启动失败");
        }else {
            Thread thread = new Thread(() -> {
                log.info("KafkaConsumer: " + this.getTopic() + "开始监听......");
                try {
                    this.consumer.consumeTopics(Collections.singletonList(this.getTopic()),getCustomBatchRecordHandler());
                }catch (Exception e){
                    log.error("KafkaConsumer: " + this.getTopic() + "消费消息失败");
                }
            });
            thread.start();
        }
    }
}
