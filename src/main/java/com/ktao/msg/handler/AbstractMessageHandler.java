package com.ktao.msg.handler;

import com.ktao.msg.listener.CustomBatchRecordHandler;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import static org.apache.kafka.common.requests.DeleteAclsResponse.log;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 22:06
 * @Description:
 */
public abstract class AbstractMessageHandler implements CustomBatchRecordHandler {
    @Override
    public void callbackBatch(ConsumerRecords<String, String> records) {
        try {
            this.handleBatchMessage(records);
        } catch (Exception var3) {
            log.info("处理消息出现异常 {}", var3);
        }
    }
    protected abstract void handleBatchMessage(ConsumerRecords<String, String> var1) throws Exception;
}
