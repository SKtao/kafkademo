package com.ktao.msg.handler;

import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.stereotype.Component;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 21:11
 * @Description:
 */
@Component
public class TestSendHandler implements CustomSendDataHandler {
    @Override
    public void sendDataCallback(RecordMetadata metadata, Exception exception) {
        System.out.println("发送成功： "+ metadata.toString());
    }
}
