package com.ktao.msg.handler;

import org.apache.kafka.clients.producer.RecordMetadata;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 21:00
 * @Description:
 */
@FunctionalInterface
public interface CustomSendDataHandler {
    void sendDataCallback(RecordMetadata metadata, Exception exception);
}
