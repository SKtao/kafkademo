package com.ktao.msg.consumer;

import com.ktao.msg.listener.CustomBatchRecordHandler;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 19:37
 * @Description:
 */
public class MsgConsumer {

    private KafkaConsumer<String, String> consumer;
    private CustomBatchRecordHandler customBatchRecordHandler;

    public MsgConsumer(Properties properties){
        this.consumer = new KafkaConsumer<String, String>(properties);
    }
    public void consumeTopics(List<String> topics, CustomBatchRecordHandler handler){
        consumer.subscribe(topics);
        customBatchRecordHandler = handler;

        while (true){
            fetchRecords();
        }
    }

    private void fetchRecords() {
        ConsumerRecords records = consumer.poll(100);
        if (!records.isEmpty()){
            customBatchRecordHandler.callbackBatch(records);
        }
    }

}
