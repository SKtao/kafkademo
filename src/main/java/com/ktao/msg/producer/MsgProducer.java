package com.ktao.msg.producer;

import com.ktao.msg.handler.CustomSendDataHandler;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 20:57
 * @Description:
 */
public class MsgProducer {
    private KafkaProducer<String, String> producer;

    public MsgProducer(Properties properties){
        if (properties != null && !properties.isEmpty()){
            this.producer = new KafkaProducer<String, String>(properties);
        }
    }
    public void sendData(ProducerRecord record, CustomSendDataHandler handler){
        this.producer.send(record, ((recordMetadata, exception) -> handler.sendDataCallback(recordMetadata, exception)));
    }

    public KafkaProducer<String, String> getProducer() {
        return producer;
    }

    public void flush(){
        if (this.producer != null){
            this.producer.flush();
        }
    }

    public void close(){
        if (this.producer != null){
            this.producer.close();
        }
    }
}
