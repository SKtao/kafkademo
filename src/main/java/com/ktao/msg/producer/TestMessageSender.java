package com.ktao.msg.producer;
import	java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author: kongtao
 * @Date: 2019-07-16 19:28
 * @Description:
 */
@Component
public class TestMessageSender {

    @Resource
    private MessageSender messageSender;

    @Value("${kafka.topic.producer.test}")
    private String topic;

    private Properties properties;

    public boolean sendMessage(String key, String value){
        ProducerMessage message = ProducerMessage.builder()
                .topic(topic)
                .key(key)
                .record(value)
                .build();
        messageSender.sendMessage(message);
        return true;
    }
}
