package com.ktao.msg.producer;

import com.ktao.msg.handler.CustomSendDataHandler;
import lombok.*;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.Serializable;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 19:24
 * @Description:
 */
@AllArgsConstructor
@Data
@Getter
@Setter
@Builder
public class ProducerMessage implements Serializable{
    private String topic;
    private String key;
    private String record;
    private CustomSendDataHandler handler;

    public ProducerRecord<String, String> getProducerRecord(){
        return new ProducerRecord<>(topic, key, record);
    }
}
