package com.ktao.msg.producer;

import com.ktao.msg.handler.CustomSendDataHandler;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;

import static org.apache.kafka.common.requests.DeleteAclsResponse.log;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 19:17
 * @Description:
 */
@Component
public class MessageSender {
    private Properties properties;

    public boolean sendMessage(ProducerMessage message){
        MsgProducer producer = null;
        try {
            producer = new MsgProducer(getProperties());
        } catch (IOException e) {
            log.warn("KafkaProducer初始化失败:",e);
            throw new RuntimeException(e.getMessage());
        }
        log.info("准备发送消息,topic={},key={},record={}",message.getTopic(),message.getKey(),message.getRecord().toString());
        ProducerRecord<String, String> producerRecord = message.getProducerRecord();
        try {
            producer.sendData(producerRecord, (RecordMetadata metadata, Exception exception) -> {
                log.info("消息发送: {}", message.getRecord().toString() );
                if (exception != null && !exception.getMessage().isEmpty()){
                    //TODO 处理消息发送异常
                    log.error("消息发送异常", exception);
                    throw new RuntimeException(exception.getMessage());
                }
                CustomSendDataHandler handler = message.getHandler();
                if (handler != null){
                    handler.sendDataCallback(metadata, exception);
                }
            });
        }catch (Exception e){
            log.error("消息发送异常", e);
            throw new RuntimeException(e.getMessage());
        }
        //若不close或sleep，topic中找不到消息
        producer.close();
        return true;
    }
    private Properties getProperties() throws IOException {
        if (properties == null) {
            ClassPathResource resource = new ClassPathResource("kafka.properties");
            properties = PropertiesLoaderUtils.loadProperties(resource);
            log.info("加载kafka配置文件：{}", properties.toString());
        }
        return properties;
    }
}
