package com.ktao.msg.controller;

import com.ktao.msg.producer.TestMessageSender;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: kongtao
 * @Date: 2019-07-14 22:27
 * @Description:
 */
@RestController
@RequestMapping(value = "/kafka")
public class TestController {

    @Resource
    private TestMessageSender sender;

    @RequestMapping(value = "/sendData", method = RequestMethod.GET)
    public void sendData(String key, String value){
        sender.sendMessage(key, value);
    }
}
